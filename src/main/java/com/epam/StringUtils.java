package com.epam;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StringUtils {
    List<Object> strs;

    public StringUtils(Object ... strs) {
    this.strs = new ArrayList();
    this.strs = Arrays.asList(strs);
    }

    public String concat(){
        return strs.stream().map(Object::toString).reduce(" ", (a, b) -> a+b);
    }
}
