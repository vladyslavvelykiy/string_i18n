package com.epam;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class App {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(4);
        StringUtils su = new StringUtils(new String("f"), new String("fas"), list);
        System.out.println(su.concat());
    }
}